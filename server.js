var express=require('express');
var nodemailer = require("nodemailer");
var bodyParser = require("body-parser");
var app=express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + '/img'));

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 3000;

/*
	Here we are configuring our SMTP Server details.
	STMP is mail server which is responsible for sending and recieving email.
*/
var smtpTransport = nodemailer.createTransport("SMTP",{

    host: 'smtp.gmail.com',
    port: 587,
    auth: {
        user: 'domain@gmail.com',
        pass: 'password'
    },
    tls: {rejectUnauthorized: false},
    debug:true
});
/*------------------SMTP Over-----------------------------*/

/*------------------Routing Started ------------------------*/
app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type,X-Requested-With');
    next();
});

app.get('/',function(req,res){
    console.log("Dummy");
    res.render('index');
});

app.post('/sendEmail',function(req,res){
    var data = req.body;
    
    var mailOptions={
        from: "Nickname <domain@gmail.com>",
        to : data.contactEmail,
        subject : "Welcome to XYZ!",
        //If can make this htnl into a file will be better!
        html: '<img src="cid:image1" style="width:400px"> \
               <h1 style="color:blue"> <strong>Welcome to XYZ</strong> </h1> \
               <p> Thank you for subscribing to XYZ\'s newsletter. </p> \
               <p>Visit us at <a href="http://www.domain.com">XYZ</a></p> \
               <p> Thank you </p>',
        attachments: [{
            fileName: 'xyz.png',
            filePath: "http://lorempixel.com/400/200/",
            cid: "image1" //same cid value as in the html img src
        }]
    };
    console.log(mailOptions);

    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("BIG ERROR");
            console.log(error);
            res.end("Email cannot be sent! :(");
        }else{
            console.log("Message sent: " + response.message);
            if(port == process.env.PORT)
            {
                console.log("Heroku successfully to send out email!");
            }
            res.end("Email successfully sent! :)");
        }
    });        
});

/*--------------------Routing Over----------------------------*/

app.listen(port,function(){
    console.log("Express Started on http//localhost:" + port);
});